import React from "react";
import { Switch, Route } from "react-router-dom";
import routes from "../../routes";
import PrivateRoute from "../PrivateRoute";

const AppRouter: React.FC = () => (
  <Switch>
    {routes.map(route =>
      route.isPrivate ? (
        <PrivateRoute
          exact={route.isExact}
          component={route.component}
          path={route.path}
          key={route.path}
        />
      ) : (
        <Route
          exact={route.isExact}
          component={route.component}
          path={route.path}
          key={route.path}
        />
      )
    )}
  </Switch>
);

export default AppRouter;
