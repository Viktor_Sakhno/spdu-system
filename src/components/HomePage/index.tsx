import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import MediaCard from "../MediaCard";
import { getCourses } from "../../containers/Questions/services/actions";
import { coursesSelector } from "../../containers/Questions/services/selectors";
import "./index.css";

const HomePage = () => {
  const dispatch = useDispatch();
  const courses = useSelector(coursesSelector);

  useEffect(() => {
    dispatch(getCourses());
  }, []);

  return (
    <div className="home-page">
      {courses.map(course => (
        <MediaCard
          title={course.title}
          imgUrl={course.imageUrl}
          text={course.description}
          link={`/courses/${course.id}`}
          key={course.id}
        />
      ))}
    </div>
  );
};
export default HomePage;
