import React from "react";
import { Route, RouteProps, Redirect } from "react-router-dom";
const PrivateRoute: React.FC<RouteProps> = props => {
  return localStorage.getItem("token") ? (
    <Route {...props} />
  ) : (
    <Redirect to="/login" />
  );
};

export default PrivateRoute;
