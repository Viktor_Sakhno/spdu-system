import { createAction } from "@reduxjs/toolkit";
import { AppDispatch } from "../../../store/store";
import { Credentials, SignUpCredentials, User } from "./typedef";
import apiCall from "../../../utils/apiCall";

export const signInRequest = createAction("SIGN_IN_REQUEST");
export const signInSuccess = createAction<User>("SIGN_IN_SUCCESS");
export const signInFailure = createAction("SIGN_IN_FAILURE");

export const signOutSuccess = createAction("SIGN_OUT_SUCCESS");

export const signUpRequest = createAction("SIGN_UP_REQUEST");
export const signUpSuccess = createAction<User>("SIGN_UP_SUCCESS");
export const signUpFailure = createAction("SIGN_UP_FAILURE");

export const signIn = (credentials: Credentials) => async (
  dispatch: AppDispatch
) => {
  dispatch(signInRequest());
  return apiCall
    .signIn(credentials)
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .then(json => dispatch(signInSuccess(json)))
    .catch(() => dispatch(dispatch(signInFailure())));
};

export const signOut = () => async (dispatch: AppDispatch) => {
  dispatch(signOutSuccess());
};

export const signUp = (credentials: SignUpCredentials) => async (
  dispatch: AppDispatch
) => {
  dispatch(signUpRequest());
  return apiCall
    .signUp(credentials)
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .then(json => dispatch(signUpSuccess(json)))
    .catch(() => dispatch(dispatch(signUpFailure())));
};
