import { createReducer } from "@reduxjs/toolkit";
import {
  signInRequest,
  signInSuccess,
  signInFailure,
  signOutSuccess,
  signUpRequest,
  signUpSuccess,
  signUpFailure
} from "./actions";

interface InitialState {
  isFetching?: boolean;
  user: string | null;
  error?: boolean;
}

const username = localStorage.getItem("user");

const initialState: InitialState = {
  isFetching: false,
  user: username ? username : null,
  error: false
};

export const auth = createReducer(initialState, builder =>
  builder
    .addCase(signInRequest, state => ({
      ...state,
      isFetching: true
    }))
    .addCase(signInSuccess, (state, action) => ({
      ...state,
      isFetching: false,
      user: action.payload.username
    }))
    .addCase(signInFailure, state => ({
      ...state,
      isFetching: false,
      error: true
    }))
    .addCase(signOutSuccess, state => ({
      ...state,
      user: null
    }))
    .addCase(signUpRequest, state => ({
      ...state,
      isFetching: true
    }))
    .addCase(signUpSuccess, (state, action) => ({
      ...state,
      isFetching: false,
      user: action.payload.username
    }))
    .addCase(signUpFailure, state => ({
      ...state,
      isFetching: false,
      error: true
    }))
);
