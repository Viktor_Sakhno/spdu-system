import { RootState } from "../../../store/rootReducer";

export const userNameSelector = (state: RootState) => state.auth.user;
