export interface User {
  username: string;
  token: string;
}

export interface Credentials {
  username: string;
  password: string;
}

export interface SignUpCredentials extends Credentials {
  email: string;
  firstName: string;
  lastName: string;
}
