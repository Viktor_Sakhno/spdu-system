import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { lecturesSelector } from "../../services/selectors";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { Button } from "@material-ui/core";
import { useFormik } from "formik";
import { addQuestion } from "../../services/actions";
import "./index.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: "70%"
      }
    }
  })
);

export default function AddQuestion() {
  const lectures = useSelector(lecturesSelector);
  const dispatch = useDispatch();
  const classes = useStyles();
  const { values, handleChange, handleSubmit } = useFormik({
    initialValues: {
      lecture: lectures[0].id,
      title: "",
      description: ""
    },
    onSubmit: values => {
      const { lecture, title, description } = values;
      dispatch(addQuestion(String(lecture), { title, body: description }));
    }
  });

  return (
    <form className={classes.root} noValidate onSubmit={handleSubmit}>
      <div className="add-form">
        <p>Ask question</p>
        <TextField
          id="lecture"
          select
          label="Choose a lecture"
          value={values.lecture}
          onChange={handleChange}
          SelectProps={{
            native: true
          }}
          helperText="Please select your lecture"
          variant="outlined"
        >
          {lectures.map(lecture => (
            <option key={lecture.id} value={lecture.id}>
              {lecture.title}
            </option>
          ))}
        </TextField>
        <TextField
          id="title"
          label="Title"
          placeholder="Your question"
          multiline
          variant="outlined"
          helperText="Please write your question"
          value={values.title}
          onChange={handleChange}
        />
        <TextField
          id="description"
          label="Question description"
          multiline
          rows="6"
          variant="outlined"
          helperText="Please write question desription"
          value={values.description}
          onChange={handleChange}
        />
        <Button type="submit" variant="contained" color="primary">
          Post your question
        </Button>
      </div>
    </form>
  );
}
