import React, { useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getLectures,
  getQuestions,
  getQuestionsByLectures
} from "./services/actions";
import { questionsSelector, lecturesSelector } from "./services/selectors";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 400;

const useStyles = makeStyles(theme => ({
  buttonContainer: {
    marginTop: "20px",
    paddingRight: "20px",
    display: "flex",
    justifyContent: "flex-end"
  },
  root: {
    display: "flex",
    minHeight: "80vh",
    justifyContent: "space-between",
    marginTop: "20px"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  paper: {
    marginRight: theme.spacing(2)
  },
  questionPaper: {
    width: "100%"
  },
  questionList: {
    flexDirection: "column"
  }
}));

const Questions: React.FC = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { id } = useParams();

  const lectures = useSelector(lecturesSelector);
  const questions = useSelector(questionsSelector);

  useEffect(() => {
    if (id) {
      dispatch(getLectures(id));
      dispatch(getQuestions(id));
    }
  }, [dispatch, id]);

  return (
    <>
      <div className={classes.buttonContainer}>
        <Button
          component={Link}
          to="/question/post"
          variant="contained"
          color="primary"
        >
          Ask question
        </Button>
      </div>
      <div className={classes.root}>
        <div className={classes.drawer}>
          <Paper className={classes.paper}>
            <ListSubheader component="div" id="nested-list-subheader">
              Lectures
            </ListSubheader>
            <MenuList>
              {lectures.map(lecture => (
                <MenuItem
                  key={lecture.id}
                  onClick={() =>
                    dispatch(getQuestionsByLectures(String(lecture.id)))
                  }
                >
                  {lecture.title}
                </MenuItem>
              ))}
            </MenuList>
          </Paper>
        </div>
        <Paper className={classes.questionPaper}>
          <List
            component="ul"
            aria-labelledby="nested-list-subheader"
            subheader={
              <ListSubheader component="div" id="nested-list-subheader">
                Questions
              </ListSubheader>
            }
            className={classes.questionList}
          >
            {questions.map(question => (
              <ListItem
                button
                component={Link}
                to={`/questions/${question.id}`}
                key={question.id}
              >
                <ListItemText
                  primary={question.title}
                  secondary={question.date}
                />
              </ListItem>
            ))}
          </List>
        </Paper>
      </div>
    </>
  );
};

export default Questions;
