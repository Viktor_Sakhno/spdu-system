import React from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { questionByIdSelector } from "../../services/selectors";

const QuestionDetails: React.FC = () => {
  const { id } = useParams();
  const question = useSelector(questionByIdSelector(id as string));
  console.log(question);
  return <div>Question {id}</div>;
};

export default QuestionDetails;
