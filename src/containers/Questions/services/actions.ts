import { createAction } from "@reduxjs/toolkit";
import { AppDispatch } from "../../../store/store";
import apiCall from "../../../utils/apiCall";
import { Courses, Question, Lecture } from "./typedef";

export const coursesRequest = createAction("COURSES_REQUEST");
export const coursesSuccess = createAction<Courses[]>("COURSES_SUCCESS");
export const coursesFailure = createAction("COURSES_FAILURE");

export const questionsRequest = createAction("QUESTION_REQUEST");
export const questionsSuccess = createAction<Question[]>("QUESTION_SUCCESS");
export const questionsFailure = createAction("QUESTION_FAILURE");

export const lecturesRequest = createAction("LECTURES_REQUEST");
export const lecturesSuccess = createAction<Lecture[]>("LECTURES_SUCCESS");
export const lecturesFailure = createAction("LECTURES_FAILURE");

export const addQuestionRequest = createAction("ADD_QUESTION_REQUEST");
export const addQuestionSuccess = createAction("ADD_QUESTION_SUCCESS");
export const addQuestionFailure = createAction("ADD_QUESTION_FAILURE");

export const getCourses = () => async (dispatch: AppDispatch) => {
  dispatch(coursesRequest());
  return apiCall
    .getCourses()
    .then(response => response.json())
    .then(json => dispatch(coursesSuccess(json)))
    .catch(() => dispatch(coursesFailure()));
};

export const getQuestions = (id: string) => async (dispatch: AppDispatch) => {
  dispatch(questionsRequest());
  return apiCall
    .getQuestions(id)
    .then(response => response.json())
    .then(json => dispatch(questionsSuccess(json)))
    .catch(() => dispatch(questionsFailure()));
};

export const getQuestionsByLectures = (id: string) => async (
  dispatch: AppDispatch
) => {
  dispatch(questionsRequest());
  return apiCall
    .getQuestionsByLectures(id)
    .then(response => response.json())
    .then(json => dispatch(questionsSuccess(json)))
    .catch(() => dispatch(questionsFailure()));
};

export const getLectures = (id: string) => async (dispatch: AppDispatch) => {
  dispatch(lecturesRequest());
  return apiCall
    .getLectures(id)
    .then(response => response.json())
    .then(json => dispatch(lecturesSuccess(json)))
    .catch(() => dispatch(lecturesFailure()));
};

export const addQuestion = (lectureId: string, question: Question) => async (
  dispatch: AppDispatch
) => {
  dispatch(addQuestionRequest);
  return apiCall
    .postQuestion(question, lectureId)
    .then(response => response.json())
    .then(json => dispatch(addQuestionSuccess()))
    .catch(() => dispatch(addQuestionFailure));
};
