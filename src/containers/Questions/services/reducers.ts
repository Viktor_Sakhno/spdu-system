import { createReducer } from "@reduxjs/toolkit";
import { Courses, Lecture, Question } from "./typedef";
import {
  coursesRequest,
  coursesSuccess,
  coursesFailure,
  questionsRequest,
  questionsSuccess,
  questionsFailure,
  lecturesRequest,
  lecturesSuccess,
  lecturesFailure
} from "./actions";

interface InitialState {
  isFetching?: boolean;
  courses: Courses[];
  lectures: Lecture[];
  questions: Question[];
  error?: boolean;
}

const initialState: InitialState = {
  isFetching: false,
  courses: [],
  lectures: [],
  questions: [],
  error: false
};

export const courses = createReducer(initialState, builder =>
  builder
    .addCase(coursesRequest, state => ({
      ...state,
      isFetching: true
    }))
    .addCase(coursesSuccess, (state, action) => ({
      ...state,
      isFetching: false,
      courses: action.payload
    }))
    .addCase(coursesFailure, state => ({
      ...state,
      error: true
    }))
    .addCase(questionsRequest, state => ({
      ...state,
      isFetching: true
    }))
    .addCase(questionsSuccess, (state, action) => ({
      ...state,
      isFetching: false,
      questions: action.payload
    }))
    .addCase(questionsFailure, state => ({
      ...state,
      error: true
    }))
    .addCase(lecturesRequest, state => ({
      ...state,
      isFetching: true
    }))
    .addCase(lecturesSuccess, (state, action) => ({
      ...state,
      isFetching: false,
      lectures: action.payload
    }))
    .addCase(lecturesFailure, state => ({
      ...state,
      error: true
    }))
);
