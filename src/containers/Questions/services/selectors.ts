import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../../../store/rootReducer";

export const coursesSelector = (state: RootState) => state.courses.courses;
export const lecturesSelector = (state: RootState) => state.courses.lectures;
export const questionsSelector = (state: RootState) => state.courses.questions;

export const questionByIdSelector = (id: string) =>
  createSelector(questionsSelector, questions =>
    questions.find(question => question.id === Number(id))
  );
