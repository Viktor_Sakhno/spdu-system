export interface Courses {
  id: number;
  title: string;
  description: string;
  imageUrl: string;
}

export interface Question {
  body: string;
  date?: string;
  id?: number;
  rating?: number;
  solved?: boolean;
  title: string;
}

export interface Lecture {
  description: string;
  id: number;
  title: string;
}
