import HomePage from "./components/HomePage";
import SignIn from "./containers/Auth/Modules/SignIn";
import SignUp from "./containers/Auth/Modules/SignUp";
import Questions from "./containers/Questions";
import QuestionDetails from "./containers/Questions/Modules/QuestionDetails";
import AddQuestion from "./containers/Questions/Modules/AddQuestion";

interface Routes {
  isExact?: boolean;
  isPrivate?: boolean;
  path?: string;
  component: React.FunctionComponent | React.ComponentClass;
}

const routes: Routes[] = [
  {
    isPrivate: true,
    isExact: true,
    path: "/",
    component: HomePage
  },
  {
    path: "/login",
    component: SignIn
  },
  {
    path: "/registration",
    component: SignUp
  },
  {
    isPrivate: true,
    path: "/courses/:id",
    component: Questions
  },
  {
    isPrivate: true,
    path: "/questions/:id",
    component: QuestionDetails
  },
  {
    isPrivate: true,
    path: "/question/post",
    component: AddQuestion
  }
];

export default routes;
