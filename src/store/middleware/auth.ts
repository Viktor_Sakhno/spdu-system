import { Middleware } from "@reduxjs/toolkit";
import { history } from "../../history";

export const authMiddleware: Middleware = store => next => action => {
  if (action.type === "SIGN_IN_SUCCESS" || action.type === "SIGN_UP_SUCCESS") {
    localStorage.setItem("user", action.payload.username);
    localStorage.setItem("token", action.payload.token);
    history.push("/");
  }
  if (action.type === "SIGN_OUT_SUCCESS") {
    localStorage.clear();
    history.push("/login");
  }
  if (action.type === "ADD_QUESTION_SUCCESS") {
    history.goBack();
  }
  // if (action.type === 'ADD_POSITION_SUCCESS') {
  //     history.push('/');
  // }
  // if (action.type === 'DELETE_POSITION_SUCCESS') {
  //     history.push('/');
  // }
  return next(action);
};
