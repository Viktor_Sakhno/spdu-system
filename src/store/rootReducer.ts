import { combineReducers } from "@reduxjs/toolkit";
import { auth } from "../containers/Auth/services/reducers";
import { courses } from "../containers/Questions/services/reducers";

export const rootReducer = combineReducers({ auth, courses });

export type RootState = ReturnType<typeof rootReducer>;
