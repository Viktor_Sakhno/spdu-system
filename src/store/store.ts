import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { authMiddleware } from "./middleware";
import { rootReducer, RootState } from "./rootReducer";

export const store = configureStore({
  reducer: rootReducer,
  middleware: [...getDefaultMiddleware<RootState>(), authMiddleware]
});

export type AppDispatch = typeof store.dispatch;
