import {
  SignUpCredentials,
  Credentials
} from "../containers/Auth/services/typedef";

import { Question } from "../containers/Questions/services/typedef";

const getToken = () => {
  const token = localStorage.getItem("token");
  return token ? token : null;
};

class ApiCall {
  signIn(credentials: Credentials) {
    return fetch("/api/auth/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(credentials)
    });
  }

  signUp(credentials: SignUpCredentials) {
    return fetch("/api/auth/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(credentials)
    });
  }

  getCourses() {
    return fetch("/api/courses", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
  }
  getQuestions(id: string) {
    return fetch(`/api/${id}/questions`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
  }
  getQuestionsByLectures(id: string) {
    return fetch(`/api/${id}/questions`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
  }
  getLectures(id: string) {
    return fetch(`/api/courses/${id}/lectures`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    });
  }
  postQuestion(question: Question, lecturesId: string) {
    return fetch(`/api/lectures/${lecturesId}/questions`, {
      method: "POST",
      body: JSON.stringify(question),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`
      }
    });
  }
}

export default new ApiCall();
